package com.alexishernandez.rest;

import com.alexishernandez.rest.empleados.Capacitacion;
import com.alexishernandez.rest.empleados.Empleado;
import com.alexishernandez.rest.empleados.Empleados;
import com.alexishernandez.rest.repositorios.EmpleadoDAO;
import com.alexishernandez.rest.utils.Configuracion;
import com.alexishernandez.rest.utils.Utilidades;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path = "/empleados")
public class EmpleadosController {

    @Autowired
    private EmpleadoDAO empDAO;

    @GetMapping(path = "/")
    public Empleados getEmpleados(){
        return empDAO.getAllEmpleados();
    }

    @GetMapping (path = "/{id}")
    public ResponseEntity<Empleado> getEmpleado (@PathVariable int id){
        Empleado emp = empDAO.getEmpleado(id);
        if (emp == null){
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok().body(emp);
        }
    }

    @PostMapping (path = "/")
    public ResponseEntity<Object> addEmpleado (@RequestBody Empleado emp){
        Integer id = empDAO.getAllEmpleados().getListaEmpleados().size() + 1;
        emp.setId(id);
        empDAO.addEmpleado(emp);

        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(emp.getId())
                .toUri();

        return ResponseEntity.created(location).build();
    }

    @PutMapping(path = "/", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> upEmpleado (@RequestBody Empleado emp){
        empDAO.upEmpleado(emp);
        return ResponseEntity.ok().build();
    }

    @PutMapping(path = "/{id}", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> upEmpleado (@PathVariable int id, @RequestBody Empleado emp){
        empDAO.upEmpleado(id,emp);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Object> delEmpleado(@PathVariable int id){
        String resp = empDAO.deleteEmpleado(id);
        if (resp == "OK"){
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }

    }

    @PatchMapping(path = "/{id}", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> softUpEmpleado(@PathVariable int id, @RequestBody Map<String, Object> updates){
        empDAO.softUpEmpleado(id, updates);
        return ResponseEntity.ok().build();
    }

    @GetMapping(path = "/{id}/capacitaciones")
    public ResponseEntity<List<Capacitacion>> getCapacitacionesEmpleado (@PathVariable int id){
        return ResponseEntity.ok().body(empDAO.getCapEmpleado(id));
    }

    @PostMapping (path = "/{id}/capacitaciones")
    public ResponseEntity<Object> addCapEmpleado (@PathVariable int id, @RequestBody Capacitacion cap){

        if(empDAO.addCapacitacion(id,cap)){
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }
//        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
//                .path("/{id}/capacitaciones")
//                .buildAndExpand(emp.getId())
//                .toUri();
//        return ResponseEntity.created(location).build();
    }

    @Value("${app.titulo}") private String titulo;

    @GetMapping("/titulo")
    public String getAppTitulo(){
        String modo = configuracion.getModo();
        return String.format("%s (%s)", titulo, modo);
    }

    @Autowired
    private Environment env;

    @GetMapping(path = "/autor")
    public String getAutor(){

        String conf = configuracion.getAutor();

        return conf;
        //return env.getProperty("app.autor" + conf);
    }

    @Autowired
    Configuracion configuracion;

    @GetMapping("/cadena")
    public String getCadena (@RequestParam String texto, @RequestParam String separador){
        try {
            return Utilidades.getCadena(texto, separador);
        } catch (Exception e){
            return "";
        }
    }





}


