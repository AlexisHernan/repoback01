package com.alexishernandez.rest;

import com.alexishernandez.rest.utils.BadSeparator;
import com.alexishernandez.rest.utils.EstadosPedido;
import com.alexishernandez.rest.utils.Utilidades;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@EnableConfigurationProperties
@SpringBootTest
public class EmpleadosControllerTest {

    @Autowired
    EmpleadosController empleadosController;

    @Test
    public void tetsGetCadena(){
        String correcto = "L.U.Z D.E.L S.O.L";
        String origen = "luz del sol";
        assertEquals(correcto,empleadosController.getCadena(origen,"."));
    }

    @Test
    public void testGetAutor(){
        String cadena = empleadosController.getAutor();
        assertEquals("\"Alexis Her\"", empleadosController.getAutor());
    }

    @Test
    public void testSeparadorGetCadena() {
        try {
            Utilidades.getCadena("Alexis Hernandez", ".");
            //fail("se esperaba BadSeparator");

        } catch (BadSeparator bs){}
    }

    @ParameterizedTest
    @ValueSource (ints = {1,3,5,17,-3, Integer.MAX_VALUE})
    public void testImpar (int numero){
        assertTrue(Utilidades.esImpar(numero));
    }

    @ParameterizedTest
    @ValueSource(strings = {""," "})
    public void testEstaBlanco (String texto){
        assertTrue(Utilidades.estaBlanco(texto));
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"  ","\t","\n"})
    public void testEstaBlancoCompleto (String texto){
        assertTrue(Utilidades.estaBlanco(texto));
    }


    @ParameterizedTest
    @EnumSource(EstadosPedido.class)
    public void testValorarEstadoPedido(EstadosPedido ep){
        assertTrue(Utilidades.valorandoEstadoPedido(ep));
    }
}
